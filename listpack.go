package rdb

import (
	"encoding/binary"
	"fmt"
	"math"
	"strconv"
)

func readListPackElement(buf *sliceBuffer) ([]byte, uint64, error) {
	b, err := buf.ReadByte()
	if err != nil {
		return nil, 0, err
	}

	// 0|xxxxxxx
	if b&0b10000000 == 0 {
		s := strconv.FormatUint(uint64(b&0b011111111), 10)
		return []byte(s), 1, nil
	}

	// 10|xxxxxx <string-data>
	if b&0b11000000 == 0b10000000 {
		len := uint64(b & 0b001111111)
		s, err := buf.Slice(int(len))
		return s, len + 1, err
	}

	// 110|xxxxx yyyyyyyy -- 13 bit signed integer
	if b&0b11100000 == 0b11000000 {
		b2, err := buf.ReadByte()
		if err != nil {
			return nil, 0, err
		}

		uval := (uint64(b&0b00011111) << 8) | uint64(b2)
		negstart := uint64(1 << 12)
		negmax := uint64(8191)

		var val uint64
		if uval >= negstart {
			uval = negmax - uval
			val = uval
			val = -val - 1
		} else {
			val = uval
		}

		s := strconv.FormatInt(int64(val), 10)
		return []byte(s), 2, nil
	}

	// 1110|xxxx yyyyyyyy -- string with length up to 4095
	if b&0b11110000 == 0b11100000 {
		b2, err := buf.ReadByte()
		if err != nil {
			return nil, 0, err
		}

		len := binary.LittleEndian.Uint64([]byte{b & 0b00011111, b2})
		s, err := buf.Slice(int(len))
		return s, len + 2, err
	}

	// 1111|0000 <4 bytes len> <large string>
	if b == 0b11110000 {
		b2, err := buf.Slice(4)
		if err != nil {
			return nil, 0, err
		}

		len := binary.LittleEndian.Uint64(b2)
		s, err := buf.Slice(int(len))
		return s, len + 5, err
	}

	// 1111|0001 <16 bits signed integer>
	if b == 0b11110001 {
		b2, err := buf.Slice(2)
		if err != nil {
			return nil, 0, err
		}

		uval := uint64(b2[0]) | (uint64(b2[1]) << 8)
		negstart := uint64(1 << 15)
		negmax := uint64(math.MaxInt16)

		var val uint64
		if uval >= negstart {
			uval = negmax - uval
			val = uval
			val = -val - 1
		} else {
			val = uval
		}

		s := strconv.FormatInt(int64(val), 10)
		return []byte(s), 3, nil
	}

	// 1111|0010 <24 bits signed integer>
	if b == 0b11110010 {
		b2, err := buf.Slice(3)
		if err != nil {
			return nil, 0, err
		}

		uval := uint64(b2[0]) | (uint64(b2[1]) << 8) | (uint64(b2[2]) << 16)
		negstart := uint64(1 << 23)
		negmax := uint64(math.MaxInt32 >> 8)

		var val uint64
		if uval >= negstart {
			uval = negmax - uval
			val = uval
			val = -val - 1
		} else {
			val = uval
		}

		s := strconv.FormatInt(int64(val), 10)
		return []byte(s), 4, nil
	}

	// 1111|0011 <32 bits signed integer>
	if b == 0b11110011 {
		b2, err := buf.Slice(4)
		if err != nil {
			return nil, 0, err
		}

		uval := uint64(b2[0]) | (uint64(b2[1]) << 8) | (uint64(b2[2]) << 16) | (uint64(b2[3]) << 24)
		negstart := uint64(1 << 31)
		negmax := uint64(math.MaxInt32)

		var val uint64
		if uval >= negstart {
			uval = negmax - uval
			val = uval
			val = -val - 1
		} else {
			val = uval
		}

		s := strconv.FormatInt(int64(val), 10)
		return []byte(s), 5, nil
	}

	// 1111|0100 <64 bits signed integer>
	if b == 0b11110100 {
		b2, err := buf.Slice(8)
		if err != nil {
			return nil, 0, err
		}

		uval := uint64(b2[0]) |
			(uint64(b2[1]) << 8) |
			(uint64(b2[2]) << 16) |
			(uint64(b2[3]) << 24) |
			(uint64(b2[4]) << 32) |
			(uint64(b2[5]) << 40) |
			(uint64(b2[6]) << 48) |
			(uint64(b2[7]) << 56)
		negstart := uint64(1 << 63)
		negmax := uint64(math.MaxInt64)

		var val uint64
		if uval >= negstart {
			uval = negmax - uval
			val = uval
			val = -val - 1
		} else {
			val = uval
		}

		s := strconv.FormatInt(int64(val), 10)
		return []byte(s), 5, nil
	}

	// 1111|0101 to 1111|1110 are currently not used.
	return nil, 0, fmt.Errorf("invalid zipmap entry")
}

func readListPackElementTotalLen(buf *sliceBuffer, len uint64) ([]byte, error) {
	if len < 128 {
		return buf.Slice(1)
	}
	if len < 16383 {
		return buf.Slice(2)
	}
	if len < 2097151 {
		return buf.Slice(3)
	}
	if len < 268435455 {
		return buf.Slice(4)
	}
	// <= 34359738367
	return buf.Slice(5)
}
