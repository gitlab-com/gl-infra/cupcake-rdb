// This is a very basic example of a program that implements rdb.decoder and
// outputs a human readable diffable dump of the rdb file.
package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"unicode"

	rdb "gitlab.com/gitlab-com/gl-infra/cupcake-rdb"
	"gitlab.com/gitlab-com/gl-infra/cupcake-rdb/nopdecoder"

	"github.com/google/gops/agent"
)

// text decoder

type textDecoder struct {
	nopdecoder.NopDecoder
	w io.Writer

	val int
}

// string

func (p *textDecoder) Set(key, value []byte, expiry int64) {
	fmt.Fprintf(p.w, "%s\t%q\t%d\n", "string", key, len(key)+len(value))
}

// hash

func (p *textDecoder) StartHash(key []byte, length, expiry int64) {
	p.val = 0
}

func (p *textDecoder) Hset(key, field, value []byte) {
	p.val += len(field) + len(value)
}

func (p *textDecoder) EndHash(key []byte) {
	fmt.Fprintf(p.w, "%s\t%q\t%d\n", "hash", key, len(key)+p.val)
}

// set

func (p *textDecoder) StartSet(key []byte, length, expiry int64) {
	p.val = 0
}

func (p *textDecoder) Sadd(key, member []byte) {
	p.val += len(member)
}

func (p *textDecoder) EndSet(key []byte) {
	fmt.Fprintf(p.w, "%s\t%q\t%d\n", "set", key, len(key)+p.val)
}

// list

func (p *textDecoder) StartList(key []byte, length, expiry int64) {
	p.val = 0
}

func (p *textDecoder) Rpush(key, value []byte) {
	p.val += len(value)
}

func (p *textDecoder) EndList(key []byte) {
	fmt.Fprintf(p.w, "%s\t%q\t%d\n", "list", key, len(key)+p.val)
}

// zset

func (p *textDecoder) StartZSet(key []byte, length, expiry int64) {
	p.val = 0
}

func (p *textDecoder) Zadd(key []byte, score float64, member []byte) {
	p.val += len(member)
}

func (p *textDecoder) EndZSet(key []byte) {
	fmt.Fprintf(p.w, "%s\t%q\t%d\n", "zset", key, len(key)+p.val)
}

// folded decoder

var reSep = regexp.MustCompile("[/:{}=]")
var reNum = regexp.MustCompile(";[0-9]+(;|$)")
var reHash = regexp.MustCompile(";[0-9a-f]{24,}(;|$)")
var reUuid = regexp.MustCompile(";[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}(;|$)")
var reEmptySep = regexp.MustCompile(";;+")

func processKey(prefix string, key []byte) []byte {
	key = bytes.Map(func(r rune) rune {
		if unicode.IsPrint(r) {
			return r
		}
		return -1
	}, key)

	key = reSep.ReplaceAll(key, []byte(";"))
	key = reNum.ReplaceAll(key, []byte(";$$NUMBER$1"))
	key = reNum.ReplaceAll(key, []byte(";$$NUMBER$1"))
	key = reHash.ReplaceAll(key, []byte(";$$HASH$1"))
	key = reUuid.ReplaceAll(key, []byte(";$$UUID$1"))
	key = reEmptySep.ReplaceAll(key, []byte(";"))

	key = append([]byte(prefix+";"), key...)

	return key
}

type foldedDecoder struct {
	nopdecoder.NopDecoder
	w io.Writer
	m map[string]int

	key string
	val int
}

// string

func (p *foldedDecoder) Set(key, value []byte, expiry int64) {
	p.key = string(processKey("string", key))
	p.val = len(value)
	if _, ok := p.m[p.key]; !ok {
		p.m[p.key] = 0
	}
	p.m[p.key] += len(key) + p.val
}

// hash

func (p *foldedDecoder) StartHash(key []byte, length, expiry int64) {
	p.key = string(processKey("hash", key))
	p.val = 0
}

func (p *foldedDecoder) Hset(key, field, value []byte) {
	p.val += len(field) + len(value)
}

func (p *foldedDecoder) EndHash(key []byte) {
	if _, ok := p.m[p.key]; !ok {
		p.m[p.key] = 0
	}
	p.m[p.key] += len(key) + p.val
}

// set

func (p *foldedDecoder) StartSet(key []byte, cardinality, expiry int64) {
	p.key = string(processKey("set", key))
	p.val = 0
}

func (p *foldedDecoder) Sadd(key, member []byte) {
	p.val += len(member)
}

func (p *foldedDecoder) EndSet(key []byte) {
	if _, ok := p.m[p.key]; !ok {
		p.m[p.key] = 0
	}
	p.m[p.key] += len(key) + p.val
}

// list

func (p *foldedDecoder) StartList(key []byte, length, expiry int64) {
	p.key = string(processKey("list", key))
	p.val = 0
}

func (p *foldedDecoder) Rpush(key, value []byte) {
	p.val += len(value)
}

func (p *foldedDecoder) EndList(key []byte) {
	if _, ok := p.m[p.key]; !ok {
		p.m[p.key] = 0
	}
	p.m[p.key] += len(key) + p.val
}

// zset

func (p *foldedDecoder) StartZSet(key []byte, cardinality, expiry int64) {
	p.key = string(processKey("zset", key))
	p.val = 0
}

func (p *foldedDecoder) Zadd(key []byte, score float64, member []byte) {
	p.val += len(member)
}

func (p *foldedDecoder) EndZSet(key []byte) {
	if _, ok := p.m[p.key]; !ok {
		p.m[p.key] = 0
	}
	p.m[p.key] += len(key) + p.val
}

func (p *foldedDecoder) EndRDB() {
	for k, v := range p.m {
		fmt.Fprintf(p.w, "%s %d\n", k, v)
	}
}

func maybeFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

var format = flag.String("format", "text", "must be one of: text, folded")

func main() {
	flag.Parse()

	if err := agent.Listen(agent.Options{}); err != nil {
		log.Fatal(err)
	}

	if len(flag.Args()) < 1 {
		fmt.Fprintf(os.Stderr, "usage: cupcake dump.rdb\n")
		os.Exit(1)
	}

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	var decoder rdb.Decoder

	switch *format {
	case "text":
		decoder = &textDecoder{w: out}
	case "folded":
		decoder = &foldedDecoder{
			w: out,
			m: make(map[string]int),
		}
	default:
		log.Fatal("format must be one of: text, folded")
	}

	f, err := os.Open(flag.Args()[0])
	maybeFatal(err)
	defer f.Close()

	err = rdb.Decode(f, decoder)
	maybeFatal(err)
}
